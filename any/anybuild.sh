#!/bin/bash
echo "ANY-BUILD: R3"
nowfolder="$(pwd)/"
pkgdir="$(pwd)/pkg";
srcdir="$(pwd)/src";
mkdir -p $srcdir;
mkdir -p $pkgdir;
pkgname="go-asisten"

build() {
  cd "$srcdir"
  git clone git@gitlab.com:AndrusGerman/go-asisten-core.git
  if [ $? -ne 0 ]
  then
    echo "ANY-BUILD: Error On Get Go-Asisten-Core"
    exit 1;
  fi
  git clone git@gitlab.com:AndrusGerman/go-asisten-gui.git
  if [ $? -ne 0 ]
  then
    echo "ANY-BUILD: Error On Get Go-Asisten-GUI"
    exit 1;
  fi
  # Go AsistenCore Build
  cd "$pkgname-core"
  go build -buildmode=plugin
  if [ $? -ne 0 ]
  then
    echo "ANY-BUILD: Error On Build core"
    exit 1;
  fi
  # Go AsistenGUI build
  cd "$srcdir/$pkgname-gui"
  go build
    if [ $? -ne 0 ]
  then
    echo "ANY-BUILD: Error On Build GUI"
    exit 1;
  fi
  # Go Asisten icon
  cp "$nowfolder/$pkgname-gui.desktop" $srcdir;
}


package () {
  cd "$pkgdir"
  mkdir -p "opt/$pkgname"

  # GoAsistenGUI package
  cp -Rv "$srcdir/$pkgname-gui" "$pkgdir/opt/$pkgname"

  # GoAsistenCORE package
  cp -Rv "$srcdir/$pkgname-core" "$pkgdir/opt/$pkgname"

  # GoAsistenGUI Launcher
  install -Dm644 "$srcdir/$pkgname-gui.desktop"    "$pkgdir/usr/share/applications/$pkgname-gui.desktop"

  # GoAsistenGUI binary Launch
  mkdir -p "usr/bin"
  ln -s "/opt/$pkgname/$pkgname-gui/$pkgname-gui" "$pkgdir/usr/bin/$pkgname-gui"
}

# Run functions
build
package
#Clear
rm -rf "$srcdir"
exit 0;
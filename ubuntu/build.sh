#!/bin/bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root (For build to Ubuntu)"
  exit
fi
pkgname="go-asisten"
nowfolder="$(pwd)/"
echo "UBUNTU-BUILD: R2"
# BUild go-asisten
./anybuild.sh;
if [ $? -ne 0 ]
then
  echo "UBUNTU-BUILD: Error in compile base";
  exit 1;
fi
# Create Basi Files
mkdir -p "$pkgname/DEBIAN/"
cp control "$pkgname/DEBIAN/"
mv pkg/* "$pkgname"
# permisos
chown root:root -R "$pkgname"
chmod 0755 "$pkgname/opt/$pkgname/$pkgname-gui/$pkgname-gui"
# Build To ubuntu
dpkg -b "$pkgname"
if [ $? -ne 0 ]
then
  echo "UBUNTU-BUILD: Error in build package";
  exit 1;
fi
# Clear
rm -rf "$pkgname";
exit 0;
